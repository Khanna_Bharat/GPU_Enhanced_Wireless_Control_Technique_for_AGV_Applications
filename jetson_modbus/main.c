/*
 * Copyright © 2008-2014 Stéphane Raimbault <stephane.raimbault@gmail.com>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#ifndef _MSC_VER
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "modbus.h"

/* The goal of this program is to check all major functions of
   libmodbus:
   - write_coil
   - read_bits
   - write_coils
   - write_register
   - read_registers
   - write_registers
   - read_registers

   All these functions are called with random values on a address
   range defined by the following defines.
*/
#define LOOP             	1
#define SERVER_ID       	0x0A
//#define SERVER_ID       	0x0B
#define ADDRESS_START    	0
#define ADDRESS_END     	8

/* At each loop, the program works in the range ADDRESS_START to
 * ADDRESS_END then ADDRESS_START + 1 to ADDRESS_END and so on.
 */
int main(void)
{
    modbus_t *ctx;
    int rc;
    int nb_fail;
    int nb_loop;
    int addr;
    int nb;
    uint8_t *tab_rq_bits;
    uint8_t *tab_rp_bits;
    uint16_t *tab_rq_registers;
    uint16_t *tab_rw_rq_registers;
    uint16_t *tab_rp_registers;

    /* RTU */
    printf("Opening RTU\n");
    ctx = modbus_new_rtu("/dev/ttyTHS1", 9600, 'N', 8, 1);
    modbus_set_slave(ctx, SERVER_ID);
#if 0
    /* TCP */
    ctx = modbus_new_tcp("127.0.0.1", 1502);
#endif
    modbus_set_debug(ctx, TRUE);

    printf("Modbus Connect\n");
    if (modbus_connect(ctx) == -1)
    {
        fprintf(stderr, "Connection failed: %s\n",modbus_strerror(errno));
        modbus_free(ctx);
        return -1;
    }

    /* Allocate and initialize the different memory spaces */
    nb = ADDRESS_END - ADDRESS_START;

    tab_rq_bits = (uint8_t *) malloc(nb * sizeof(uint8_t));
    memset(tab_rq_bits, 0, nb * sizeof(uint8_t));

    tab_rp_bits = (uint8_t *) malloc(nb * sizeof(uint8_t));
    memset(tab_rp_bits, 0, nb * sizeof(uint8_t));

    tab_rq_registers = (uint16_t *) malloc(nb * sizeof(uint16_t));
    memset(tab_rq_registers, 0, nb * sizeof(uint16_t));

    tab_rp_registers = (uint16_t *) malloc(nb * sizeof(uint16_t));
    memset(tab_rp_registers, 0, nb * sizeof(uint16_t));

    tab_rw_rq_registers = (uint16_t *) malloc(nb * sizeof(uint16_t));
    memset(tab_rw_rq_registers, 0, nb * sizeof(uint16_t));

    addr = ADDRESS_START;						// For test

#if 0
    nb_loop = nb_fail = 0;
    while (nb_loop++ < LOOP)
    {
        for (addr = ADDRESS_START; addr < ADDRESS_END; addr++)
        {
            int i;

            /* Random numbers (short) */
            for (i=0; i<nb; i++)
            {
                tab_rq_registers[i] = (uint16_t) (65535.0*rand() / (RAND_MAX + 1.0));
                tab_rw_rq_registers[i] = ~tab_rq_registers[i];
                tab_rq_bits[i] = tab_rq_registers[i] % 2;
            }

            nb = ADDRESS_END - addr;

            /* WRITE BIT */
            rc = modbus_write_bit(ctx, addr, tab_rq_bits[0]);
            if (rc != 1)
            {
                printf("ERROR modbus_write_bit (%d)\n", rc);
                printf("Address = %d, value = %d\n", addr, tab_rq_bits[0]);
                nb_fail++;
            }
            else
            {
                rc = modbus_read_bits(ctx, addr, 1, tab_rp_bits);
                if (rc != 1 || tab_rq_bits[0] != tab_rp_bits[0])
                {
                    printf("ERROR modbus_read_bits single (%d)\n", rc);
                    printf("address = %d\n", addr);
                    nb_fail++;
                }
            }

            /* MULTIPLE BITS */
            rc = modbus_write_bits(ctx, addr, nb, tab_rq_bits);
            if (rc != nb)
            {
                printf("ERROR modbus_write_bits (%d)\n", rc);
                printf("Address = %d, nb = %d\n", addr, nb);
                nb_fail++;
            }
            else
            {
                rc = modbus_read_bits(ctx, addr, nb, tab_rp_bits);
                if (rc != nb)
                {
                    printf("ERROR modbus_read_bits\n");
                    printf("Address = %d, nb = %d\n", addr, nb);
                    nb_fail++;
                }
                else
                {
                    for (i=0; i<nb; i++)
                    {
                        if (tab_rp_bits[i] != tab_rq_bits[i])
                        {
                            printf("ERROR modbus_read_bits\n");
                            printf("Address = %d, value %d (0x%X) != %d (0x%X)\n",
                                   addr, tab_rq_bits[i], tab_rq_bits[i],
                                   tab_rp_bits[i], tab_rp_bits[i]);
                            nb_fail++;
                        }
                    }
                }
            }

        }


    }
#endif


    printf("Modbus Read bits\n");
    int i=0;
	//rc = modbus_read_bits(ctx, addr, nb, tab_rp_bits);
	//rc = eMBFuncWriteRegisterMotor0EN(ctx, addr, nb, tab_rp_bits);
	//rc = eMBFuncWriteRegisterMotor0DEN(ctx, addr, nb, tab_rp_bits);
	//rc = eMBFuncWriteRegisterMotor1EN(ctx, addr, nb, tab_rp_bits);
	rc = eMBFuncWriteRegisterMotor1DEN(ctx, addr, nb, tab_rp_bits);


	printf("rc is %d\n",rc);
    if (rc != nb)
	{
		printf("ERROR modbus_read_bits\n");
		printf("Address = %d, nb = %d\n", addr, nb);
		nb_fail++;
	}
	else
	{
		for (i=0; i<nb; i++)
		{
			//if (tab_rp_bits[i] != tab_rq_bits[i])
			{
				//printf("ERROR modbus_read_bits\n");
				printf("Address = %d, value %d (0x%X) != %d (0x%X)\n",addr, tab_rp_bits[i]);
				//nb_fail++;
			}
		}
	}

	printf("Test: ");
	if (nb_fail)
	{
		printf("%d FAILS\n", nb_fail);
	}
	else
	{
		printf("SUCCESS\n");
	}
    /* Free the memory */
    free(tab_rq_bits);
    free(tab_rp_bits);
    free(tab_rq_registers);
    free(tab_rp_registers);
    free(tab_rw_rq_registers);

    /* Close the connection */
    modbus_close(ctx);
    modbus_free(ctx);

    return 0;
}
