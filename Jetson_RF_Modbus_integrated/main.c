/*
===============================================================================
 Name        : RF Jetson TK-1
 Author      : Bharat Khanna
 Description : RF(Sx1276) and Jetson Tk-1 communication
===============================================================================
*/


#include <stdio.h>
#include <stdbool.h>

#include "LoRa.h"
#include"common.h"

#include <string.h>
#include <unistd.h>
#include "jetsonGPIO.h"

#ifndef _MSC_VER
#include <unistd.h>
#endif
#include <stdlib.h>
#include <errno.h>
#include "modbus.h"


#define RF_Receive 1
#define TransmittACk 0
#define ack_enabled 0
#define LOOP             	1
#define SERVER_ID       	0x0A
//#define SERVER_ID       	0x0B
#define ADDRESS_START    	0
#define ADDRESS_END     	8

int rfInit(void);

char receiveData=0;
int packetSize;
modbus_t *ctx;
int addr;
int nb;
uint8_t *tab_rq_bits;
uint8_t *tab_rp_bits;
uint16_t *tab_rq_registers;
uint16_t *tab_rw_rq_registers;
uint16_t *tab_rp_registers;

static void delay_ms(unsigned int ms)
{
    unsigned int i,j;
    for(i=0;i<ms;i++)
        for(j=0;j<50000;j++);
}
int motor_modbus_init()
{
	
	ctx = modbus_new_rtu("/dev/ttyTHS1", 9600, 'N', 8, 1);
	modbus_set_slave(ctx, SERVER_ID);

	modbus_set_debug(ctx, TRUE);

	//printf("Modbus Connect\n");
	if (modbus_connect(ctx) == -1)
	{
		fprintf(stderr, "Connection failed: %s\n",modbus_strerror(errno));
		modbus_free(ctx);
		return -1;
	}

	/* Allocate and initialize the different memory spaces */
	nb = ADDRESS_END - ADDRESS_START;

	tab_rq_bits = (uint8_t *) malloc(nb * sizeof(uint8_t));
	memset(tab_rq_bits, 0, nb * sizeof(uint8_t));

	tab_rp_bits = (uint8_t *) malloc(nb * sizeof(uint8_t));
	memset(tab_rp_bits, 0, nb * sizeof(uint8_t));

	tab_rq_registers = (uint16_t *) malloc(nb * sizeof(uint16_t));
	memset(tab_rq_registers, 0, nb * sizeof(uint16_t));

	tab_rp_registers = (uint16_t *) malloc(nb * sizeof(uint16_t));
	memset(tab_rp_registers, 0, nb * sizeof(uint16_t));

	tab_rw_rq_registers = (uint16_t *) malloc(nb * sizeof(uint16_t));
	memset(tab_rw_rq_registers, 0, nb * sizeof(uint16_t));

	addr = ADDRESS_START;	
	return 0;
}

void motor_modbus_cleanup()
{
	/* Free the memory */
	free(tab_rq_bits);
	free(tab_rp_bits);
	free(tab_rq_registers);
	free(tab_rp_registers);
	free(tab_rw_rq_registers);

	/* Close the connection */
	modbus_close(ctx);
	modbus_free(ctx);	
}
int motor_modbus(int func)
{
	
	int rc;
	int nb_fail;
	int nb_loop;

	//printf("Modbus Read bits\n");
	int i=0;
	if(func == 0 )
		rc = eMBFuncWriteRegisterMotor0EN(ctx, addr, nb, tab_rp_bits);
	else if(func == 1 )
		rc = eMBFuncWriteRegisterMotor0DEN(ctx, addr, nb, tab_rp_bits);
	else if(func == 2 )
		rc = eMBFuncWriteRegisterMotor1EN(ctx, addr, nb, tab_rp_bits);
	else if(func == 3 )
		rc = eMBFuncWriteRegisterMotor1DEN(ctx, addr, nb, tab_rp_bits);
	else
		printf("Invalid motor function \n");
	//printf("rc is %d\n",rc);
	if (rc != nb)
	{
		printf("ERROR modbus_read_bits\n");
		//printf("Address = %d, nb = %d\n", addr, nb);
		nb_fail++;
	}
/*
	else
	{
		for (i=0; i<nb; i++)
		{

			printf("Address = %d, value %d (0x%X) != %d (0x%X)\n",addr, tab_rp_bits[i]);
		}
	}

	printf("Test: ");
	if (nb_fail)
	{
		printf("%d FAILS\n", nb_fail);
	}
	else
	{
		printf("SUCCESS\n");
	}
*/	
}

void  INThandler(int sig)
{
	printf("Inside signal handler\n");	
	motor_modbus_cleanup();
	exit(0);
}
/**************************************************************************************************
* main : Main program entry
**************************************************************************************************/
int main(void)
{
	motor_modbus_init();
	LoRabegin(915000000);
	int counter =0;
	signal(SIGINT, INThandler);
#if RF_Receive
	while(1)
	{
		// Check if packet is received
		packetSize = parsePacket(0);
		if (packetSize)
		{
			counter = 0;
			while (available())
			{
				counter = 0;
				receiveData = read_LoRa();
				//printf("_____%d\n",receiveData);
				if(receiveData == 'A')
				{
					motor_modbus(0);
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'B')
				{
					motor_modbus(2);
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'C')
				{
					motor_modbus(1);
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'D')
				{
					motor_modbus(3);
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'E')
				{
					motor_modbus(0);
					motor_modbus(2);
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'F')
				{
					motor_modbus(1);
					motor_modbus(3);
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}

#if 0
				else if(receiveData == 'X')
				{
					//MOTOR0_DISABLE();
					//MOTOR1_DISABLE();
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
					printf("Acknowledge Received \n");

				}
#endif
			}

		}
#if ack_enabled
		else
		{
			counter = counter +1;
			if(counter > 10000)
			{
				//MOTOR0_DISABLE();
				//MOTOR1_DISABLE();
				printf("Ack not received \n");
				printf("counter = %d \n",counter);
				counter = 0;
			}
		}
#endif
	}
#endif

#if TransmittACk
	const char buffer[] = "Data from LPC1769";
	char Acknowledgement;
	Acknowledgement = 'A';
	while(1)
	{
		printf("Start Sending data \n");
		delay_ms(1000);
		LoRabeginPacket(0);
		//writebyte(Acknowledgement);
		write_LoRa(buffer, sizeof(buffer));
		LoRaendPacket();
		printf("Data sent \n");

	}

#endif
	closespi_fd();
	close_gpioUnexport();
	motor_modbus_cleanup();
}
