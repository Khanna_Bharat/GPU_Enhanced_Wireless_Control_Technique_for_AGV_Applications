/*
 * ssp.h
 *
 *  Created on: Oct 29, 2016
 *      Author: Bharat
 */

#ifndef SSP_H_
#define SSP_H_



#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "jetsonGPIO.h"     
//jetsonGPIO DIO2_GPIO_LoRa = gpio57;

// Init SPI
void spiInit();

// close file descripter
void closespi_fd();

// Function Prototypes
uint8_t ssp1Transfer(int Read_Write_Register, uint8_t address, uint8_t value);



#endif /* SSP_H_ */
