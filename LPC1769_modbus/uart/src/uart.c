/*
===============================================================================
 Name        : uart.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#include <stdio.h>
#include<string.h>
#define ulBaudRate 9600
#define INTERNAL_CLOCK		    (4  * 1000 * 1000UL)    ///< Do not change, this is the same on all LPC17XX
#define EXTERNAL_CLOCK          (12 * 1000 * 1000UL)    ///< Change according to your board specification
#define RTC_CLOCK               (32768UL)               ///< Do not change, this is the typical RTC crystal value
#define UART_LCR_DLAB_EN        (1 << 7)		/*!< UART Divisor Latches Access bit enable */
// TODO: insert other include files here
char readchar;
// TODO: insert other definitions and declarations here
void UART3_IRQHandler()
{
	readchar = LPC_UART3->RBR;
	printf("%c", readchar);
}
static void delay_ms(unsigned int ms)
{
    unsigned int i,j;
    for(i=0;i<ms;i++)
        for(j=0;j<50000;j++);
}

void uart3_putchar(char out)
{
	// Transmitter Empty bit number 6
	// Is set when both UnTHR and UnTSR are empty
	//RBR DLL=0
	 LPC_UART3->THR = out;
	//while( !(LPC_UART3->LSR & (1 << 5)));
	//return 1;
}
unsigned int sys_get_cpu_clock()
{
	unsigned clock = 0;

	/* Determine clock frequency according to clock register values             */
	if (((LPC_SC->PLL0STAT >> 24) & 3) == 3)
	{ /* If PLL0 enabled and connected */
	    switch (LPC_SC->CLKSRCSEL & 0x03)
	    {
	        case 0: /* Int. RC oscillator => PLL0    */
	        case 3: /* Reserved, default to Int. RC  */
	            clock = (INTERNAL_CLOCK
	                    * ((2 * ((LPC_SC->PLL0STAT & 0x7FFF) + 1)))
	                    / (((LPC_SC->PLL0STAT >> 16) & 0xFF) + 1)
	                    / ((LPC_SC->CCLKCFG & 0xFF) + 1));
	            break;

	        case 1: /* Main oscillator => PLL0       */
	            clock = (EXTERNAL_CLOCK
	                    * ((2 * ((LPC_SC->PLL0STAT & 0x7FFF) + 1)))
	                    / (((LPC_SC->PLL0STAT >> 16) & 0xFF) + 1)
	                    / ((LPC_SC->CCLKCFG & 0xFF) + 1));
	            break;

	        case 2: /* RTC oscillator => PLL0        */
	            clock = (RTC_CLOCK
	                    * ((2 * ((LPC_SC->PLL0STAT & 0x7FFF) + 1)))
	                    / (((LPC_SC->PLL0STAT >> 16) & 0xFF) + 1)
	                    / ((LPC_SC->CCLKCFG & 0xFF) + 1));
	            break;
	    }
	}
	else
	{
	    switch (LPC_SC->CLKSRCSEL & 0x03)
	    {
	        case 0: /* Int. RC oscillator => PLL0    */
	        case 3: /* Reserved, default to Int. RC  */
	            clock = INTERNAL_CLOCK / ((LPC_SC->CCLKCFG & 0xFF) + 1);
	            break;
	        case 1: /* Main oscillator => PLL0       */
	            clock = EXTERNAL_CLOCK / ((LPC_SC->CCLKCFG & 0xFF) + 1);
	            break;
	        case 2: /* RTC oscillator => PLL0        */
	            clock = RTC_CLOCK / ((LPC_SC->CCLKCFG & 0xFF) + 1);
	            break;
	    }
	}

	return clock;
}

void init()
{
	unsigned int baud = 0;
	const unsigned int pclk = sys_get_cpu_clock();
    //simISR.attach_us(&prvvUARTISR,1000);    // Cam - attach prvvUARTISR to a 1mS ticker to simulate serial interrupt behaviour
                                            // 1mS is just short of a character time at 9600 bps, so quick enough to pick
                                            // up status on a character by character basis.
	printf("Initializing serial port\n");
	//const uint32_t  Baud_Rate = 9600;

	// Step 1: UART3 power/clock control bit(24) of PCONP
	LPC_SC->PCONP &=~ (1<<25);
	LPC_SC->PCONP |= (1<<25);

	// Step 2: PCLKSEL1 bit 16 and 17 of Peripheral Clock Selection register1 for Uart2, controls the rate of the clock signal
	// that will be supplied to the corresponding peripheral Function peripheral clock selection
	// 00 CCLK/4
	// 01 CCLK
	// 10 CCLK/4
	// 11 CLK/4
	LPC_SC->PCLKSEL1 &=~(3<<18);
	LPC_SC->PCLKSEL1 |= (1<<18);

	// Step 3:// table 280
	//LPC_UART2->LCR &=~ (3<<0 | 1<<7);
	LPC_UART3->LCR = 0x03;
	// LCR bit o and 1, Word length select
	//LPC_UART2->LCR |= (3<<0);						//00 5-bit character length
													//01 6-bit character length
													//10 7-bit character length
													//11 8-bit character length
	//Chip_UART_SetBaud(LPC_UART2,ulBaudRate);
#if 1
	//Enable, Divisor latch access bit(DLAB)
	LPC_UART3->LCR |= (1<<7);
	baud = (pclk / (16 * ulBaudRate));
	printf("serial port Initialized\n");
	//LPC_UART2->DLL = (baud & 0xFF);
	//LPC_UART2->DLM = (baud >> 8);

	LPC_UART3->DLL = 250;
	LPC_UART3->DLM = 2;

	LPC_UART3->LCR &=  ~(1<<7);
#endif
	LPC_UART3->FCR = 0x07;
	LPC_UART3->FCR &= ~(3<<6);
	//LPC_UART2->DLM = 0x00;
	//LPC_UART3->DLL =0x38;
	//LPC_UART2->DLL =( sys_get_cpu_clock() / (16 * ulBaudRate));

	// Step 4: FIFO not use:
	LPC_PINCON->PINSEL0 &=  ~(3<<0 | 3<<2);
	LPC_PINCON->PINSEL0 |= (1<<1 | 1<<3);    //TXD3 | RXD3
	LPC_PINCON->PINMODE0 &=  ~(3<<0 | 3<<2);
	LPC_PINCON->PINMODE0 |= (3<<2);           // 11 pull down

	// Step 6: Disable, Divisor latch access bit(DLAB)

	NVIC_EnableIRQ(UART3_IRQn);

	//enable receive data available interrupt for uart2
	LPC_UART3->IER &=~ (3<<0) ;
	LPC_UART3->IER |= (1<<0);
	//LPC_UART2->IER = (1 << 0) | (1 << 2); // B0:Rx, B1: Tx

}
int main(void) {

	char readchar;
	init();
	int i;
	char myString[] = "Hey\n";
	int length = strlen(myString);
	while(1)
	{
		for( i = 0 ; i < length ; i++ )
		{
			char writechar = myString[i];
			uart3_putchar(writechar);
			delay_ms(100);
		}


	}

    return 0 ;
}
