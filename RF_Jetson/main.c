/*
===============================================================================
 Name        : RF Jetson TK-1
 Author      : Bharat Khanna
 Description : RF(Sx1276) and Jetson Tk-1 communication
===============================================================================
*/


#include <stdio.h>
#include <stdbool.h>

#include "LoRa.h"
#include"common.h"


#include <string.h>
#include <unistd.h>
#include "jetsonGPIO.h"


#define RF_Receive 1
#define TransmittACk 0
#define ack_enabled 0
int rfInit(void);

char receiveData=0;
int packetSize;


static void delay_ms(unsigned int ms)
{
    unsigned int i,j;
    for(i=0;i<ms;i++)
        for(j=0;j<50000;j++);
}


/**************************************************************************************************
* main : Main program entry
**************************************************************************************************/
int main(void)
{
	LoRabegin(915000000);
	int counter =0;
#if RF_Receive
	while(1)
	{
		// Check if packet is received
		packetSize = parsePacket(0);
		if (packetSize)
		{
			counter = 0;
			while (available())
			{
				counter = 0;
				receiveData = read_LoRa();
				//printf("_____%d\n",receiveData);
				if(receiveData == 'A')
				{
					//MOTOR0_ENABLE();
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'B')
				{
					//MOTOR1_ENABLE();
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'C')
				{
					//MOTOR0_DISABLE();
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'D')
				{
					//MOTOR1_DISABLE();
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'E')
				{
					//MOTOR0_ENABLE();
					//MOTOR1_ENABLE();
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}
				else if(receiveData == 'F')
				{
					//MOTOR0_DISABLE();
					//MOTOR1_DISABLE();
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
				}

#if 0
				else if(receiveData == 'X')
				{
					//MOTOR0_DISABLE();
					//MOTOR1_DISABLE();
					printf("Received packet '");
					printf("%c",receiveData);
					printf("' with RSSI ");
					printf("%d\n",packetRssi());
					printf("Acknowledge Received \n");

				}
#endif
			}

		}
#if ack_enabled
		else
		{
			counter = counter +1;
			if(counter > 10000)
			{
				//MOTOR0_DISABLE();
				//MOTOR1_DISABLE();
				printf("Ack not received \n");
				printf("counter = %d \n",counter);
				counter = 0;
			}
		}
#endif
	}
#endif

#if TransmittACk
	const char buffer[] = "Data from LPC1769";
	char Acknowledgement;
	Acknowledgement = 'A';
	while(1)
	{
		printf("Start Sending data \n");
		delay_ms(1000);
		LoRabeginPacket(0);
		//writebyte(Acknowledgement);
		write_LoRa(buffer, sizeof(buffer));
		LoRaendPacket();
		printf("Data sent \n");

	}

#endif
	closespi_fd();
	close_gpioUnexport();
}
