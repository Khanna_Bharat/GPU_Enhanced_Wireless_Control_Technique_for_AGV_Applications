/*
 * spi.cpp
 *
 *  Created on: Oct 29, 2016
 *      Author: Bharat
 */

#include "ssp.h"

#define Read_written_Register 0

static int fd;
struct spi_ioc_transfer xfer[2];
void spiInit()
{
		fd = open("/dev/spidev0.0", O_RDWR);
		if (fd < 0) 
		{
			perror("open");
			//return 1;
		}
// spi interupt enable how?
}

void closespi_fd()
{
	close(fd);
}
/****************************************************************************************
* ssp1Transfer - Transmit and receive byte on SSP1
****************************************************************************************/


uint8_t ssp1Transfer(int Read_Write_Register, uint8_t address, uint8_t value)
{
	uint8_t dummy=0;
	//unsigned char dummy=0; // this also works fine
	if(Read_Write_Register == 1)
	{
		int status;
		uint8_t buf[4];
		memset(xfer, 0, sizeof xfer);
		memset(buf, 0, sizeof buf);

		buf[0] = address;

		xfer[0].tx_buf = (unsigned long)buf;
		xfer[0].len = 1;

		xfer[1].rx_buf = (unsigned long) buf;
		xfer[1].len = 1;

		status = ioctl(fd, SPI_IOC_MESSAGE(2), xfer);

		if (status < 0) 
		{
			perror("SPI_IOC_MESSAGE");
			return;
		}
		usleep(100);
		dummy = *buf;
			//printf(" I am in the ssp.c file, Dummy byte = %02x \n",dummy);

	}
	else if(Read_Write_Register == 2)
	{
		int status;
		uint8_t buf[4];
		memset(xfer, 0, sizeof xfer);
		memset(buf, 0, sizeof buf);

		buf[0] = address;
		buf[1] = value;

		xfer[0].tx_buf = (unsigned long)buf;
		xfer[0].len = 2;


		status = ioctl(fd, SPI_IOC_MESSAGE(1), xfer);

		if (status < 0) 
		{
			perror("SPI_IOC_MESSAGE");
			return;
		}
		usleep(100);
	}
	else
	{
		printf("In ssp function neither Read nor write Mode = %d\n",Read_Write_Register );
	}

	return dummy;
}

