/*
 * LoRa.h
 *
 *  Created on: Oct 29, 2016
 *      Author: Bharat
 */

#ifndef LORA_H_
#define LORA_H_

#include <stddef.h>
#include <stdint.h>
#include "jetsonGPIO.h"     

  void digitalWrite(uint8_t value);
  void gpioInit();
  void close_gpioUnexport();


  int LoRabegin(long frequency);
  void end();
  int LoRabeginPacket(int implicitHeader);
  int LoRaendPacket();

  int parsePacket(int size );
  int packetRssi();
  float packetSnr();

  // from Print
  size_t writebyte(uint8_t byte);
  size_t write_LoRa(const uint8_t *buffer, size_t size);

  // from Stream
  int available();
  int read_LoRa();
  int peek();
  void flush();

  void onReceive(void(*callback)(int));

  void receive(int size);
  void idle_LoRa();
  void sleep_LoRa();

  void setTxPower(int level);
  void setFrequency(long frequency);
  void setSpreadingFactor(int sf);
  void setSignalBandwidth(long sbw);
  void setCodingRate4(int denominator);
  void setPreambleLength(long length);
  void setSyncWord(int sw);
  void crc();
  void noCrc();

  uint8_t random();



 // void dumpRegisters(Stream& out);
uint8_t readRegister(uint8_t address);

  void explicitHeaderMode();
  void implicitHeaderMode();

  void handleDio0Rise();


  void writeRegister(uint8_t address, uint8_t value);
  uint8_t singleTransfer(int Read_Write_Register, uint8_t address, uint8_t value);

  static void onDio0Rise();




#endif /* LORA_H_ */
