#include "hidjoystickrptparser.h"
#include <LoRa.h>
#define LORA_start 1
int count =0;
JoystickReportParser::JoystickReportParser(JoystickEvents *evt) :
joyEvents(evt),
oldHat(0xDE),
oldButtons(0) {
        for (uint8_t i = 0; i < RPT_GEMEPAD_LEN; i++)
                oldPad[i] = 0xD;
}

void JoystickReportParser::Parse(USBHID *hid, bool is_rpt_id, uint8_t len, uint8_t *buf) {
        bool match = true;

        // Checking if there are changes in report since the method was last called
        for (uint8_t i = 0; i < RPT_GEMEPAD_LEN; i++)
                if (buf[i] != oldPad[i]) {
                        match = false;
                        break;
                }

        // Calling Game Pad event handler
        if (!match && joyEvents) {
                joyEvents->OnGamePadChanged((const GamePadEventData*)buf);

                for (uint8_t i = 0; i < RPT_GEMEPAD_LEN; i++) oldPad[i] = buf[i];
        }

        uint8_t hat = (buf[5] & 0xF);

        // Calling Hat Switch event handler
        if (hat != oldHat && joyEvents) {
                joyEvents->OnHatSwitch(hat);
                oldHat = hat;
        }

        uint16_t buttons = (0x0000 | buf[6]);
        buttons <<= 4;
        buttons |= (buf[5] >> 4);
        uint16_t changes = (buttons ^ oldButtons);

        // Calling Button Event Handler for every button changed
        if (changes) {
                for (uint8_t i = 0; i < 0x0C; i++) {
                        uint16_t mask = (0x0001 << i);

                        if (((mask & changes) > 0) && joyEvents) {
                                if ((buttons & mask) > 0)
                                        joyEvents->OnButtonDn(i + 1);
                                else
                                        joyEvents->OnButtonUp(i + 1);
                        }
                }
                oldButtons = buttons;
        }
}

void JoystickEvents::OnGamePadChanged(const GamePadEventData *evt) {
        /*
        Serial.print("X1: ");
        PrintHex<uint8_t > (evt->X, 0x80);
        Serial.print("\tY1: ");
        PrintHex<uint8_t > (evt->Y, 0x80);
        Serial.print("\tX2: ");
        PrintHex<uint8_t > (evt->Z1, 0x80);
        Serial.print("\tY2: ");
        PrintHex<uint8_t > (evt->Z2, 0x80);
        Serial.print("\tRz: ");
        PrintHex<uint8_t > (evt->Rz, 0x80);
        Serial.println("");
        */
}

void JoystickEvents::OnHatSwitch(uint8_t hat) {
        Serial.print("Hat Switch: ");
        PrintHex<uint8_t > (hat, 0x80);
        Serial.println("");
}

void JoystickEvents::OnButtonUp(uint8_t but_id) {
        Serial.print("Up: ");
        Serial.println(but_id, DEC);
  //if(count == 0 )
  //{
  #if LORA_start
  // working frequency range from 724E6 to 1040E6.
    //if (!LoRa.begin(1040E6)) { 
    //if (!LoRa.begin(1020E6)) { 
    //if (!LoRa.begin(724E6)) { 
    //if (!LoRa.begin(725E6)) { 
    //if (!LoRa.begin(750E6)) {
    //if (!LoRa.begin(790E6)) {
    //if (!LoRa.begin(800E6)) {
    //if (!LoRa.begin(845E6)) {
    //if (!LoRa.begin(850E6)) {
    //if (!LoRa.begin(910E6)) {
    //if (!LoRa.begin(868E6)) {
    if (!LoRa.begin(915E6)) {
      Serial.println("Starting LoRa failed!");
      while (1);
    } 
    #endif
    //count =1;
  //}
       uint8_t buffer=0;
        switch (but_id){
          case 1:
          if(count == 0)
          {
            count++;
            break;
          }
            Serial.println("Up: button pressed");
            LoRa.beginPacket();
            buffer ='A';
            LoRa.write(buffer);
            LoRa.endPacket();
            delay(200);
            break;
          case 2:
            Serial.println("Right: button pressed ");
            LoRa.beginPacket();
            buffer ='B';
            LoRa.write(buffer);
            LoRa.endPacket();
            delay(200);
            break;
          case 3:
            Serial.println("Down: button pressed  ");
            LoRa.beginPacket();
            buffer ='C';
            LoRa.write(buffer);
            LoRa.endPacket();
            delay(200);
            break;
          case 4:
            Serial.println("Left: button pressed");
            LoRa.beginPacket();
            buffer ='D';
            LoRa.write(buffer);
            LoRa.endPacket();
            delay(200);
            break;
          case 6:
            Serial.println("Both ON: button pressed");
            LoRa.beginPacket();
            buffer ='E';
            LoRa.write(buffer);
            LoRa.endPacket();
            delay(200);
            break;
          case 8:
            Serial.println("Both OFF: button pressed");
            LoRa.beginPacket();
            buffer ='F';
            LoRa.write(buffer);
            LoRa.endPacket();
            delay(200);
            break;
          default:
            Serial.println("Other: button pressed");
            break;
        }

        //LoRa.end();
        //delay(100);
        
}

void JoystickEvents::OnButtonDn(uint8_t but_id) {
        Serial.print("Dn: ");
        Serial.println(but_id, DEC);
}
