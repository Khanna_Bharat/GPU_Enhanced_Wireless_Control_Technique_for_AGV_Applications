#include <hiduniversal.h>
#include <usbhid.h>
#include <usbhub.h>
#include <LoRa.h>
#include "hidjoystickrptparser.h"
#include "Timer.h"
/******************************************** Receive *******************************************/
#define RF_Receive 0
#define RF_Init 0
/******************************************** Transmit *******************************************/
#define USB_start_main 1
#define RF_send 1
/******************************************** ACK *******************************************/
#define ack_start_stop 0

USB Usb;
USBHub Hub(&Usb);
HIDUniversal Hid(&Usb);
JoystickEvents JoyEvents;
JoystickReportParser Joy(&JoyEvents);
int counter = 0;
Timer *timer1 = new Timer(500); //will call the callback in the interval of 500 ms

bool Receive_data()
{ 
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    Serial.print("Received packet '");
    while (LoRa.available()) {
      Serial.print((char)LoRa.read());
    }
    Serial.print("' with RSSI ");
    Serial.println(LoRa.packetRssi());
  }
  return true;
}

void send_ACK()
{
  const uint8_t buffer[]="XX";
  LoRa.beginPacket();
  LoRa.write(buffer, sizeof(buffer));
  //Serial.println("buffer");
  LoRa.endPacket();
  //delay(400);
}

void setup() {
  Serial.begin(9600);
#if ack_start_stop
  timer1->setOnTimer(&send_ACK); //callback function
  timer1->Start(); //start the thread.
#endif 
  while (!Serial);
  Serial.println("LoRa sender");
  Serial.println("Start");
#if USB_start_main 
   if (Usb.Init() == -1)
      Serial.println("OSC did not start."); 
   delay(200);
   if (!Hid.SetReportParser(0, &Joy))
      ErrorMessage<uint8_t > (PSTR("SetReportParser"), 1);
#endif

#if RF_Init
// working frequency range from 724E6 to 1040E6.
//if (!LoRa.begin(1040E6)) {
//if (!LoRa.begin(1020E6)) {
//if (!LoRa.begin(724E6)) {
//if (!LoRa.begin(725E6)) {
//if (!LoRa.begin(750E6)) {
//if (!LoRa.begin(790E6)) {
//if (!LoRa.begin(800E6)) {
//if (!LoRa.begin(845E6)) {
//if (!LoRa.begin(850E6)) {
//if (!LoRa.begin(910E6)) {
//if (!LoRa.begin(868E6)) {
if (!LoRa.begin(915E6)) {
      Serial.println("Starting LoRa failed!");
      while (1);
    } 
#endif
}

void loop() {
#if USB_start_main 
  Usb.Task();
#endif

#if RF_Receive
  Receive_data();
#endif

#if ack_start_stop
  timer1->Update();
#endif
  
}
