/*
 * motor.h
 *
 *  Created on: Sep 29, 2016
 *      Author: Bharat
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#define MOTOR0_ENABLE()			(LPC_GPIO2->FIOCLR = (0x1<<11))
#define MOTOR0_DISABLE()		(LPC_GPIO2->FIOSET = (0x1<<11))

#define MOTOR1_ENABLE()			(LPC_GPIO2->FIOCLR = (0x1<<12))
#define MOTOR1_DISABLE()		(LPC_GPIO2->FIOSET = (0x1<<12))

int motor_init();
void delay_ms(unsigned int ms);

#endif /* MOTOR_H_ */
