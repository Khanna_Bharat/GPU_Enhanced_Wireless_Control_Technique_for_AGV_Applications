/*
 * motor.c
 *
 *  Created on: Oct 15, 2016
 *      Author: Bharat
 */



/*
===============================================================================
 Name        : pwm.c
 Author      : Bharat
 Description : PWM code for LPC1769
===============================================================================
*/



#include <stdio.h>
#include "motor.h"

// TODO: insert other include files here

// TODO: insert other definitions and declarations here

int motor_init()
{
// Initializing P0.11
	LPC_PINCON->PINSEL4 &=~ (3 << 22);

	LPC_PINCON->PINMODE4 = 0;
	LPC_PINCON->PINMODE4 |= (3 << 22);

	LPC_GPIO2->FIODIR |= (1 << 11);
	LPC_GPIO2->FIOCLR &=~ (1 << 11);

// Initializing P0.12
	LPC_PINCON->PINSEL4 &=~ (3 << 24);

	LPC_PINCON->PINMODE4 = 0;
	LPC_PINCON->PINMODE4 |= (3 << 24);

	LPC_GPIO2->FIODIR |= (1 << 12);
	LPC_GPIO2->FIOCLR &=~ (1 << 12);

	return 0;
}

void delay_ms(unsigned int ms)
{
    unsigned int i,j;
    for(i=0;i<ms;i++)
        for(j=0;j<50000;j++);
}
