#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <unistd.h>
#include "jetsonGPIO.h" 
jetsonGPIO DIO2_GPIO_LoRa = gpio57;

void gpioInit()  			
{
    gpioExport(DIO2_GPIO_LoRa);
    gpioSetDirection(DIO2_GPIO_LoRa,outputPin);
    gpioSetValue(DIO2_GPIO_LoRa,low);
}

void digitalWrite(uint8_t value)
{
	//printf("Pin : %d, value %d\n",DIO2_GPIO_LoRa,value);
	if(value == 1)
	{
    		gpioSetValue(DIO2_GPIO_LoRa, on);    
	}
	else if(value == 0)
	{
    		gpioSetValue(DIO2_GPIO_LoRa, off);
	}
}

static void dumpstat(const char *name, int fd)
{
	__u8	mode, lsb, bitsr=8, bitsw;
	__u32	speed=100000;

	if (ioctl(fd, SPI_IOC_RD_MODE, &mode) < 0) {
		perror("SPI rd_mode");
		return;
	}
	if (ioctl(fd, SPI_IOC_RD_LSB_FIRST, &lsb) < 0) {
		perror("SPI rd_lsb_fist");
		return;
	}
	if (ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bitsr) < 0) {
		perror("SPI bits_per_word");
		return;
	}
	if (ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bitsr) < 0) {
		perror("SPI bits_per_word");
		return;
	}
	if (ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bitsw) < 0) {
		perror("SPI bits_per_word");
		return;
	}
	if (ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed) > 0) {
		perror("SPI write max_speed_hz");
		return;
    }
	if (ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0) {
		perror("SPI max_speed_hz");
		return;
	}

	printf("%s: spi mode %d, %d bitsr, %d bitsw, %sper word, %d Hz max\n",
		name, mode, bitsr, bitsw, lsb ? "(lsb first) " : "", speed);
}


int main()
{
	while(1)	
	{
		//unsigned char dummy=0;
		uint8_t dummy=0;
		int fd;
		struct spi_ioc_transfer xfer[2];
		unsigned char buf[1], *bp;
		int len, status;
		gpioInit();
		digitalWrite(0);
		usleep(1000);
		digitalWrite(1);
		usleep(1000);
		uint8_t	address = 0x42;
		address &= 0x7f; 
		fd = open("/dev/spidev0.0", O_RDWR);
		if (fd < 0) 
		{
			perror("open");
			return 1;
		}
		memset(xfer, 0, sizeof xfer);
		memset(buf, 0, sizeof buf);
		/*
		* Send a GetID command
		*/
		buf[0] = address;
		//buf[0] = 0x9f;
		xfer[0].tx_buf = (unsigned long)buf;
		xfer[0].len = 1;

		xfer[1].rx_buf = (unsigned long) buf;
		xfer[1].len = 1;

		status = ioctl(fd, SPI_IOC_MESSAGE(2), xfer);

		if (status < 0) 
		{
			perror("SPI_IOC_MESSAGE");
			return;
		}
		usleep(100);
		//printf("response(%d): ", status);
		//for (bp = buf; len; len--)
		//	printf("%02x ", *bp++);
		dummy = *buf;
		printf("dummy = %x ", dummy);
		printf("\n");
		usleep(5000);
		gpioUnexport(DIO2_GPIO_LoRa);
		close(fd);
	}	
}
