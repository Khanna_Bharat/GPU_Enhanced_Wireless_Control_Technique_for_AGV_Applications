################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/lwIP/core/ipv4/autoip.c \
../Source/lwIP/core/ipv4/icmp.c \
../Source/lwIP/core/ipv4/igmp.c \
../Source/lwIP/core/ipv4/inet.c \
../Source/lwIP/core/ipv4/inet_chksum.c \
../Source/lwIP/core/ipv4/ip.c \
../Source/lwIP/core/ipv4/ip_addr.c \
../Source/lwIP/core/ipv4/ip_frag.c 

OBJS += \
./Source/lwIP/core/ipv4/autoip.o \
./Source/lwIP/core/ipv4/icmp.o \
./Source/lwIP/core/ipv4/igmp.o \
./Source/lwIP/core/ipv4/inet.o \
./Source/lwIP/core/ipv4/inet_chksum.o \
./Source/lwIP/core/ipv4/ip.o \
./Source/lwIP/core/ipv4/ip_addr.o \
./Source/lwIP/core/ipv4/ip_frag.o 

C_DEPS += \
./Source/lwIP/core/ipv4/autoip.d \
./Source/lwIP/core/ipv4/icmp.d \
./Source/lwIP/core/ipv4/igmp.d \
./Source/lwIP/core/ipv4/inet.d \
./Source/lwIP/core/ipv4/inet_chksum.d \
./Source/lwIP/core/ipv4/ip.d \
./Source/lwIP/core/ipv4/ip_addr.d \
./Source/lwIP/core/ipv4/ip_frag.d 


# Each subdirectory must supply rules for building sources it contributes
Source/lwIP/core/ipv4/%.o: ../Source/lwIP/core/ipv4/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS-Plus-IO/Include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS/include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS-Plus-IO/Device/LPC17xx/SupportedBoards" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS/portable/GCC/ARM_CM3" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS-Plus-CLI" -I"/home/zeus/LPCXpresso/workspace/CMSISv2p00_LPC17xx/inc" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/FatFS" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/lwIP/include/ipv4" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/lwIP/netif/include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/lwIP/lwIP_Apps" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/lwIP/include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/Examples/Include" -I"/home/zeus/LPCXpresso/workspace/lpc17xx.cmsis.driver.library/Include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source" -Os -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -Wextra -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Source/lwIP/core/ipv4/inet_chksum.o: ../Source/lwIP/core/ipv4/inet_chksum.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS-Plus-IO/Include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS/include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS-Plus-IO/Device/LPC17xx/SupportedBoards" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS/portable/GCC/ARM_CM3" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Products/FreeRTOS-Plus-CLI" -I"/home/zeus/LPCXpresso/workspace/CMSISv2p00_LPC17xx/inc" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/FatFS" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/lwIP/include/ipv4" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/lwIP/netif/include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/lwIP/lwIP_Apps" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/lwIP/include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source/Examples/Include" -I"/home/zeus/LPCXpresso/workspace/lpc17xx.cmsis.driver.library/Include" -I"/home/zeus/LPCXpresso/workspace/FreeRTOS-Plus-Demo-2/Source" -Os -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -Wextra -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"Source/lwIP/core/ipv4/inet_chksum.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


