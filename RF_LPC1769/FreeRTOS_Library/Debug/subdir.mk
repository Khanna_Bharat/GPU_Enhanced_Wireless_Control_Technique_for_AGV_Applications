################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../list.c \
../queue.c \
../tasks.c 

OBJS += \
./list.o \
./queue.o \
./tasks.o 

C_DEPS += \
./list.d \
./queue.d \
./tasks.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -I"/home/zeus/Desktop/git_practice/GPU_Enhanced_Wireless_Control_Technique_for_AGV_Applications/RF_LPC1769/FreeRTOS_Library/include" -I"/home/zeus/Desktop/git_practice/GPU_Enhanced_Wireless_Control_Technique_for_AGV_Applications/RF_LPC1769/FreeRTOS_Library/portable" -I"/home/zeus/Desktop/git_practice/GPU_Enhanced_Wireless_Control_Technique_for_AGV_Applications/RF_LPC1769/SimpleDemo" -O1 -g3 -Wall -c -fmessage-length=0 -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


