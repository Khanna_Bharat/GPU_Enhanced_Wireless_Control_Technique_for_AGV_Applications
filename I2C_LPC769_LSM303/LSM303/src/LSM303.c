/*
===============================================================================
 Name        : i2c_test.c
 Author      : Bharat Khanna
 Version     : 1.0
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif
#include <LPC17xx.h>
#include "stdlib.h"
#include "string.h"
#include <cr_section_macros.h>
//#include "i2c_17xx_40xx.h"
#define SPEED_100KHZ         100000
#define SPEED_400KHZ         400000
#define INTERNAL_CLOCK		 (4  * 1000 * 1000UL)    ///< Do not change, this is the same on all LPC17XX
#define EXTERNAL_CLOCK       (12 * 1000 * 1000UL)    ///< Change according to your board specification
#define RTC_CLOCK            (32768UL)               ///< Do not change, this is the typical RTC crystal value

// IC2 control bits
#define AA      		(1 << 2)
#define SI      		(1 << 3)
#define STO     		(1 << 4)
#define STA     		(1 << 5)
#define I2EN    		(1 << 6)

/*
 * MCP7940N Register Address
 */
#define  ADDR_SEC        0x00

/**************************************************************************************************
* LSM303 Accelerometer Register Address
**************************************************************************************************/
#define LSM_303_ACCEL_ADDR	(0x19)
#define	CTRL_REG1_A		(0x20)	/*	control register 1		*/
#define	CTRL_REG2_A		(0x21)	/*	control register 2		*/
#define	CTRL_REG3_A		(0x22)	/*	control register 3		*/
#define	CTRL_REG4_A		(0x23)	/*	control register 4		*/
#define	CTRL_REG5_A		(0x24)	/*	control register 5		*/
#define	CTRL_REG6_A		(0x25)	/*	control register 6		*/

#define OUT_X_L_A       (0x28)
#define OUT_X_H_A       (0x29)
#define OUT_Y_L_A       (0x2A)
#define OUT_Y_H_A       (0x2B)
#define OUT_Z_L_A       (0x2C)
#define OUT_Z_H_A       (0x2D)


/**************************************************************************************************
* LSM303 Magnetometer Register Address
**************************************************************************************************/
#define LSM_303_MAGNET_ADDR	(0x1E)
#define CRA_REG_M 		(0x00)
#define CRB_REG_M		(0x01)
#define MR_REG_M		(0x02)
#define OUT_X_H_M		(0x03)
#define OUT_X_L_M		(0x04)
#define OUT_Z_H_M		(0x05)
#define OUT_Z_L_M		(0x06)
#define OUT_Y_H_M		(0x07)
#define OUT_Y_L_M		(0x08)
#define SR_REG_M 		(0x09)
#define TEMP_OUT_H_M	(0x31)
#define TEMP_OUT_L_M	(0x32)

/**************************************************************************************************
* LSM303 Accelerometer Register Address
**************************************************************************************************/

static volatile uint8_t  slave_address; // formatted by send or receive
static volatile uint8_t* buf;
static volatile uint32_t buf_len;
static volatile uint32_t num_transferred;
static volatile uint32_t i2c1_busy;
uint32_t ignore_data_nack = 1;
uint8_t i2c_status_buf[100];
uint32_t i2c_status_pos;
uint16_t xM=0,yM=0,zM=0;

/**************************************************************************************************
* i2c1_send 	:	Triggers start condition and waits for the completion of transmission
**************************************************************************************************/
uint32_t i2c1_send(uint8_t address, uint8_t* buffer, uint32_t length) {
    // check software FSM
    if (i2c1_busy)
        //error_led_trap(0x11000001, i2c0_busy, 0, 0, 0, 0, 0, 0, 0);
        return 0;

    // set to status to 'busy'
    i2c1_busy = 1;

    // setup pointers
    slave_address = (address<<1);
    buf = buffer;
    buf_len = length;
    num_transferred = 0;

    // trigger a start condition
    LPC_I2C1->I2CONSET = STA;

    // wait for completion
    while (i2c1_busy);

    // get how many bytes were transferred
    return num_transferred;
}

/**************************************************************************************************
* i2c1_receive	:	Triggers start condition and waits for the completion of receive
**************************************************************************************************/
uint32_t i2c1_receive(uint8_t address, uint8_t* buffer, uint32_t length)
{
    // check software FSM
    if (i2c1_busy)
        //error_led_trap(0x11000002, i2c0_busy, 0, 0, 0, 0, 0, 0, 0);
        return 0;

    // set to status to 'busy'
    i2c1_busy = 1;

    // setup pointers
    slave_address = (uint8_t)((address<<1)|0x01);
    buf = buffer;
    buf_len = length;
    num_transferred = 0;

    // trigger a start condition
    LPC_I2C1->I2CONSET = STA;

    // wait for completion
    while (i2c1_busy);

    // get how many bytes were transferred
    return num_transferred;
}
/**************************************************************************************************
* I2C1_IRQHandler	:	I2C1 Interrupt handler
**************************************************************************************************/
void I2C1_IRQHandler(void)
{
    // get reason for interrupt
    uint8_t status = LPC_I2C1->I2STAT;

    // ignore data nack when control is true
    if ((status == 0x30) && (ignore_data_nack))
            status = 0x28;

    // LPC17xx User Manual page 443:
    //      "...read and write to [I2DAT] only while ... the SI bit is set"
    //      "Data in I2DAT remains stable as long as the SI bit is set."


    /**************************************DEBUG************************************************************/
    i2c_status_buf[i2c_status_pos] = status;
    i2c_status_pos++;
    if (i2c_status_pos > 99)
        i2c_status_pos = 0;
    /**************************************DEBUG************************************************************/


    switch(status) {

    // Int: start condition has been transmitted
    // Do:  send SLA+R or SLA+W
    case 0x08:
    	LPC_I2C1->I2DAT = slave_address; // formatted by send or receive
    	LPC_I2C1->I2CONCLR = STA | SI;
        break;

    // Int: repeated start condition has been transmitted
    // Do:  send SLA+R or SLA+W
    //case 0x10:
    //    regs->I2DAT = slave_address;
    //    regs->I2CONCLR = STA | SI;
    //    break;

    // Int: SLA+W has been transmitted, ACK received
    // Do:  send first byte of buffer if available
    case 0x18:
        if (num_transferred < buf_len)
        {
        	LPC_I2C1->I2DAT = buf[0];
        	LPC_I2C1->I2CONCLR = STO | STA | SI;
        }
        else
        {
        	LPC_I2C1->I2CONCLR = STA | SI;
        	LPC_I2C1->I2CONSET = STO;
        }
        break;

    // Int: SLA+W has been transmitted, NACK received
    // Do:  stop!
    case 0x20:
    	LPC_I2C1->I2CONCLR = STA | SI;
    	LPC_I2C1->I2CONSET = STO;
        num_transferred = 0xFFFFFFFF;
        i2c1_busy = 0;
        break;

    // Int: data byte has been transmitted, ACK received
    // Do:  load next byte if available, else stop
    case 0x28:
        num_transferred++;
        if (num_transferred < buf_len)
        {
        	LPC_I2C1->I2DAT = buf[num_transferred];
        	LPC_I2C1->I2CONCLR = STO | STA | SI;
        }
        else
        {
        	LPC_I2C1->I2CONCLR = STA | SI;
        	LPC_I2C1->I2CONSET = STO;
        	i2c1_busy = 0;
        }
        break;

    // Int: data byte has been transmitted, NACK received
    // Do:  stop!
    case 0x30:
    	LPC_I2C1->I2CONCLR = STA | SI;
    	LPC_I2C1->I2CONSET = STO;
    	i2c1_busy = 0;
        break;

    // Int: arbitration lost in SLA+R/W or Data bytes
    // Do:  release bus
    case 0x38:
    	LPC_I2C1->I2CONCLR = STO | STA | SI;
    	i2c1_busy = 0;
        break;

    // Int: SLA+R has been transmitted, ACK received
    // Do:  determine if byte is to be received
    case 0x40:
        if (num_transferred < buf_len)
        {
        	LPC_I2C1->I2CONCLR = STO | STA | SI;
        	LPC_I2C1->I2CONSET = AA;
        }
        else
        {
        	LPC_I2C1->I2CONCLR = AA | STO | STA | SI;
        }
        break;

    // Int: SLA+R has been transmitted, NACK received
    // Do:  stop!
    case 0x48:
    	LPC_I2C1->I2CONCLR = STA | SI;
    	LPC_I2C1->I2CONSET = STO;
        num_transferred = 0xFFFFFFFF;
        i2c1_busy = 0;
        break;

    // Int: data byte has been received, ACK has been returned
    // Do:  read byte, determine if another byte is needed
    case 0x50:
        buf[num_transferred] = LPC_I2C1->I2DAT;
        num_transferred++;
        if (num_transferred < buf_len)
        {
        	LPC_I2C1->I2CONCLR = STO | STA | SI;
            LPC_I2C1->I2CONSET = AA;
        }
        else
        {
        	LPC_I2C1->I2CONCLR = AA | STO | STA | SI;
        }
        break;

    // Int: data byte has been received, NACK has been returned
    // Do:  transfer is done, stop.
    case 0x58:
    	LPC_I2C1->I2CONCLR = STA | SI;
        LPC_I2C1->I2CONSET = STO;
        i2c1_busy = 0;
        break;

    // something went wrong, trap error
    default:
        while (1); // flash a LED or something😦
        break;

    }
}
/**************************************************************************************************
* sys_get_cpu_clock	:	Returns the system clock frequency
**************************************************************************************************/
unsigned int sys_get_cpu_clock()
{
	unsigned clock = 0;

	/* Determine clock frequency according to clock register values             */
	if (((LPC_SC->PLL0STAT >> 24) & 3) == 3)
	{ /* If PLL0 enabled and connected */
	    switch (LPC_SC->CLKSRCSEL & 0x03)
	    {
	        case 0: /* Int. RC oscillator => PLL0    */
	        case 3: /* Reserved, default to Int. RC  */
	            clock = (INTERNAL_CLOCK * ((2 * ((LPC_SC->PLL0STAT & 0x7FFF) + 1)))
	                    / (((LPC_SC->PLL0STAT >> 16) & 0xFF) + 1)
	                    / ((LPC_SC->CLKSRCSEL & 0xFF) + 1));
	            break;

	        case 1: /* Main oscillator => PLL0       */
	            clock = (EXTERNAL_CLOCK
	                    * ((2 * ((LPC_SC->PLL0STAT & 0x7FFF) + 1)))
	                    / (((LPC_SC->PLL0STAT >> 16) & 0xFF) + 1)
	                    / ((LPC_SC->CLKSRCSEL & 0xFF) + 1));
	            break;

	        case 2: /* RTC oscillator => PLL0        */
	            clock = (RTC_CLOCK
	                    * ((2 * ((LPC_SC->PLL0STAT & 0x7FFF) + 1)))
	                    / (((LPC_SC->PLL0STAT >> 16) & 0xFF) + 1)
	                    / ((LPC_SC->CLKSRCSEL & 0xFF) + 1));
	            break;
	    }
	}
	else
	{
	    switch (LPC_SC->CLKSRCSEL & 0x03)
	    {
	        case 0: /* Int. RC oscillator => PLL0    */
	        case 3: /* Reserved, default to Int. RC  */
	            clock = INTERNAL_CLOCK / ((LPC_SC->CLKSRCSEL & 0xFF) + 1);
	            break;
	        case 1: /* Main oscillator => PLL0       */
	            clock = EXTERNAL_CLOCK / ((LPC_SC->CLKSRCSEL & 0xFF) + 1);
	            break;
	        case 2: /* RTC oscillator => PLL0        */
	            clock = RTC_CLOCK / ((LPC_SC->CLKSRCSEL & 0xFF) + 1);
	            break;
	    }
	}

	return clock;
}

/**************************************************************************************************
* i2cinit	:	Initializes the i2c1 on LPC1769
**************************************************************************************************/
void i2cinit()
{
	uint32_t SCLValue;

	// Power up the I2C2 module
	LPC_SC->PCONP |= (1<<19);

	// Disable both pull-up and pull-down p0.0 & p0.1
	LPC_PINCON->PINMODE0 &= ~(0xF << 0);
	LPC_PINCON->PINMODE0 |=  (0xA << 0);

	// Enable open drain on both pins
	LPC_PINCON->PINMODE0 |= (3<<0);

	// Select pin P0.0 ->SDA1 and P0.1->SCL1
	LPC_PINCON->PINSEL0 |= (3<<0);
	LPC_PINCON->PINSEL0 |= (3<<2);

	LPC_SC->PCLKSEL1 |= (3<<6);

	LPC_I2C1->I2CONCLR =  0x6C;	// Clear all I2c flags

	// Clock setting
	SCLValue = ((sys_get_cpu_clock()/ 8) / SPEED_100KHZ);
	LPC_I2C1->I2SCLH = (uint32_t) (SCLValue >> 1);;
	LPC_I2C1->I2SCLH = (uint32_t) (SCLValue - (LPC_I2C1->I2SCLH));

	// Chip_Clock_GetPeripheralClockRate
	LPC_I2C1->I2ADR0 = 0;
	LPC_I2C1->I2ADR1 = 0;
	LPC_I2C1->I2ADR2 = 0;
	LPC_I2C1->I2ADR3 = 0;

	// Enable I2C1
	LPC_I2C1->I2CONSET = 0x40;

	// Enable I2C1 interrupt
	NVIC_EnableIRQ(I2C1_IRQn);
}

/**************************************************************************************************
* Main Entry
* 	-> Initializes I2c1 module
* 	-> Configures Magnetometer in LSP303
* 	-> Reads X,Y,Z data from Magnetometer
**************************************************************************************************/
int main(void)
{
	uint8_t buf[100] = {};					// Read data buffer
	uint8_t slave = LSM_303_MAGNET_ADDR;

    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();

    printf("Initializing i2c\n");

    // Initializing i2c1
    i2cinit();

    // Configuring LSM303 Magnetometer
	buf[0]= CRA_REG_M;
	buf[1] = 0x10;
	i2c1_send(slave, buf, 2);

	buf[0]= MR_REG_M;
	buf[1] = 0x00;
	i2c1_send(slave, buf, 2);

	while(1)
	{
		buf[0]= OUT_X_H_M;
		i2c1_send(slave, buf, 1);
		i2c1_receive(slave, buf, 1);

		xM = buf[0];
		xM = (xM << 8);

		buf[0]= OUT_X_L_M;
		i2c1_send(slave, buf, 1);
		i2c1_receive(slave, buf, 1);

		xM |= buf[0];

		printf("X: %d\n",xM);

		buf[0]= OUT_Z_H_M;
		i2c1_send(slave, buf, 1);
		i2c1_receive(slave, buf, 1);

		yM = (buf[0]);
		yM = (yM << 8);

		buf[0]= OUT_Z_L_M;
		i2c1_send(slave, buf, 1);
		i2c1_receive(slave, buf, 1);

		yM |= buf[0];
		printf("Y: %d\n",yM);

		buf[0]= OUT_Y_H_M;
		i2c1_send(slave, buf, 1);
		i2c1_receive(slave, buf, 1);
		zM = (buf[0]);
		zM = (zM <<8);

		buf[0]= OUT_Y_L_M;
		i2c1_send(slave, buf, 1);
		i2c1_receive(slave, buf, 1);
		zM |= buf[0] ;
		printf("Z: %d\n",zM);
    }
    return 0 ;
}
